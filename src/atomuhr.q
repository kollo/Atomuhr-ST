; ################################################################
;  Projekt Atomuhr DCF-77 Treiber    (c) Markus Hoffmann   V.1.07a
;  Letzte �nderung:  11.03.1995
; ################################################################
VERSION=$107
XB_ID='dcf7'         ; XBRA-Identifikation
;
; Es wird eine neue Joystick-Routine installiert
; Die Zeit wird im Gemdos-Variablenbereich gesetzt
; Das Programm benutzt selbstmodifizioerenden Code,
; belegt weniger als 1Kbytes Speicher.
; Compilat: TEXT: 966 Bytes

; Der Treiber belegt folgende System-Variablen:

; $3EC L Magic (wenn hier 'dcf7' steht, ist die Zeit im Gemdos g�ltig)
; $3F0 L Pointer auf Variablentabelle
; $3F4 W Zeit im Gemdosformat
; $3F6 W Datum im Gemdosformat
; $3f8 L DCFBIT0G   Letztes G�ltiges DCF
; $3fC L DCFBIT1G

; Codierung der Zeitinformation:

; Bit 0      M     (immer 0) Minutenanfang
; Bit 1-14   X     Codierung bei Bedarf
; Bit 15     R     (1=Reserveantenne)
; Bit 16     A1    (1=Wechsel sommer/Winterzeit)
; Bit 17,18  Z1,Z2 Zeitzone (MESZ=1,0  MEZ=0,1)
; Bit 19     A2    (1=Schaltsekunde in dieser Minute)
; Bit 20     S     (Immer 1) Begin der Zeitinformation
; Bit 21-24  Minute Einer
; Bit 25-27  Minute Zehner
; Bit 28     P1     Paritaet Bits 21-27
; Bit 29-32  Stunde Einer
; Bit 33,34  Stunde Zehner
; Bit 35     P2     Paritaet Bits 29-34
; Bit 36-39  Tag Einer
; Bit 40,41  Tag Zehner
; Bit 42-44  Wochentag: 001=Mo ... 111=So
; Bit 45-48  Monat Einer
; Bit 49     Monat Zehner
; Bit 50-53  Jahr Einer
; Bit 54-57  Jahr Zehner
; Bit 58     P3     Paritaet Bits 36-57
; (bit 59    Schaltsekunde immer 0)


XPOINT=$3F0
XMAGIC=$3EC
XTIME=$3f4
XDATE=$3f6
XDCF0=$3f8
XDCF1=$3fc


Begin:    BRA      STARTUP   ; wird �berschrieben von:
 
      ;   DC.w    VERSION; -6 Versionsnr.
      ;   dc.l    XB_ID  ; -4 Erkennungscode
TABELLE:             ; Offsets in der Tabelle:
DCFBIT1:  Dc.l    0      ; 0       ; Hier werden die gerade empfangenen
DCFBIT0:  Dc.l    0      ; 4       ; Daten abgespeichert, auch falsche !
sec:      dc.w    0      ; 8       ; Sekunden-Impulsz�hler
sig:      dc.w    0      ; 10      ; �bertragenes Bit (0,-1)
sync:     dc.w    0      ; 12      ; wenn -1, war die Letzte Minute erfolgreich
datea:    dc.l $609e     ; 14
timea:    dc.l $4e0e     ; 18
TIMERon:  dc.l    0      ; 22      ;
timeroff: dc.l    0      ; 26      ; intere Vars
old_status: dc.w  0      ; 30      ; Das ist das Signal live (0/1)
 ; Hier folgt noch die Zeit in einer TM-Datenstruktur
tm:
tm_sec:   dc.w    0      ; 32
tm_min:   dc.w    0      ; 34
tm_hour:  dc.w    0      ; 36
tm_mday:  dc.w    0      ; 38
tm_mon:   dc.w    0      ; 40
tm_year:  dc.w    0      ; 42
tm_wday:  dc.w    0      ; 44    Wochentag (1=Mon ... 7 = Son)
tm_yday:  dc.w    0      ; 46    Tag im Jahr (0-366)
tm_isdst: dc.w    -1     ; 48    ?  Zeitzone  ?

    INCLUDE "..\atomuhr\new_joy.q"
    illegal

zuend:     ; bis hierher bleibt alles im Speicher einschl Basepage

INFOTXT:  DC.B   27,'p DCF-77 Treibersoftware V.1.07b nur f�r TOS 1.0/1.4 ! (c) Markus Hoffmann',27,'q',13,10,0
          align

startup:  lea     (begin-128)(pc),a0    ; Damit nichts verschwendet
          lea     infotxt(pc),a1     ; wird, kopiere etwas in
          MOVEQ   #(STARTUP-INFOTXT)/2-1,D5  ; DIE �BERFL�SSIGE BASEPAGE
\copy     move    (a1)+,(a0)+
          dbra    d5,\copy
          PEA     (begin-128)(PC)          ; PRINT
          move    #9,-(sp)
          trap    #1
          addq.l  #6,sp

          CLR.L   -(A7)
          MOVE.W  #$20,-(A7)    ; super
          TRAP    #1
          addq.l  #6,sp
          lea     SSP_sav(pc),a0
          MOVE.L  D0,(a0)

          lea     tabelle(pc),a1
          move.l  #XB_ID,-4(a1)        ; Erkennunsmarke
          move    #VERSION,-6(a1)       ; Versionsnummer
          move.l  $4,a0            ; �berpr�fe die TOS-Version
          move.l  #$4e0e,(timea-tabelle)(a1)    ; timea
          move.l  #$609e,(datea-tabelle)(a1)    ; datea
          cmp.l   #$fc0020,a0
          BEQ.s   TOS1
          move.l  #$378a,(timea-tabelle)(a1)    ; timea
          move.l  #$60be,(datea-tabelle)(a1)    ; datea

TOS1:     lea     (XMAGIC).S,a0    ; Zeit von vor Reset �bernehmen
          cmp.l   #'dcf7',(a0)     ; wenn g�ltig
          bne.s   ttw
          move.l  timea(pc),a0
          move    XTIME,(a0)
          move.l  datea(pc),a0
          move    XDATE,(a0)

ttw:      MOVE.W  #$22,-(A7) ; KBDVBASE
          TRAP    #$E 
          addq.l  #2,sp
          MOVEA.L D0,A5 


          move.l 24(a5),a0
          move.l (w2-new_joy)(a0),d0
          lea    new_joy(pc),a0
          move.l (w2-new_joy)(a0),d1
          cmp.l  d0,d1
          beq.s  ist_schon

          MOVE    SR,D1 
          ORI.W   #$700,SR

INS_ROUT: lea     new_joy(pc),a0
          MOVE.L  24(A5),(old_joy-new_joy)(a0)
          MOVE.L  a0,24(A5) 
          MOVE    D1,SR 
          lea     tabelle(pc),a0
          move.l  a0,XPOINT

VERLASSE: MOVE.L  SSP_SAV(PC),-(A7) ; SUPER ende
          MOVE.W  #$20,-(A7)
          TRAP    #1
          ADDQ.L  #6,A7 
ENDE:
          clr     -(sp)             ; Keepterm
          MOVE.L  #($100+zuend-begin),-(A7)
          MOVE.W  #$31,-(A7)
          TRAP    #1

IST_SCHON: MOVE.L  SSP_SAV(PC),-(A7) ; SUPER ende
           MOVE.W  #$20,-(A7)
           TRAP    #1
           pea   errmel1(pc)   ; PRINT
           move  #9,-(sp)
           trap  #1
           clr   (sp)          ; TERM
           trap  #1

errmel1:  dc.b "Der DCF-77 Treiber ist schon aktiv! Installierung abgebrochen.",13,10,0
          align
SSP_SAV:  Dc.L    0


       END

 