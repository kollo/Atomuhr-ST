      ;     DC.w    $0106   ; -6 Versionsnr
      ;    dc.l   'dcf7'   ; -4 Erkennungscode
TABELLE:             ; Offsets in der Tabelle:
DCFBIT1:  Dc.l    0      ; 0       ; Hier werden die gerade empfangenen
DCFBIT0:  Dc.l    0      ; 4       ; Daten abgespeichert, auch falsche !
sec:      dc.w    0      ; 8       ; Sekunden-Impulsz�hler
sig:      dc.w    0      ; 10      ; �bertragenes Bit (0,-1)
sync:     dc.w    0      ; 12      ; wenn -1, war die Letzte Minute erfolgreich
datea:    dc.l $609e     ; 14
timea:    dc.l $4e0e     ; 18
TIMERon:  dc.l    0      ; 22      ;
timeroff: dc.l    0      ; 26      ; intere Vars
old_status: dc.w  0      ; 30      ; Das ist das Signal live (0/1)
 ; Hier folgt nocht die Zeit in einer TM-Datenstruktur
tm:
tm_sec:   dc.w    0      ; 32
tm_min:   dc.w    0      ; 34
tm_hour:  dc.w    0      ; 36
tm_mday:  dc.w    0      ; 38
tm_mon:   dc.w    0      ; 40
tm_year:  dc.w    0      ; 42
tm_wday:  dc.w    0      ; 44    Wochentag (0=Son ... 6 = Sam)
tm_yday:  dc.w    0      ; 46    Tag im Jahr (0-366)
tm_isdst: dc.w    -1     ; 48    ?  Zeitzone  ?

 