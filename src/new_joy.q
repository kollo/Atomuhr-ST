; Modul f�r ATOMUHR Hauptroutine Wird eingebunden

NEW_JOY:  CMPI.B   #-1,(A0)
          BNE      JUMP
          movem.l  d0-a6,-(sp)
          MOVE.B   2(A0),D0
          MOVE.B   old_status(pc),D3
          EOR.B    D0,D3
          BTST     #0,D3
          BEQ.s    OLD4

          lea     tabelle(pc),a5
          MOVE.L  $4BA.S,D1     ; Timer
          MOVE.B  D0,(old_status-tabelle)(a5)

          btst    #0,d0
          beq.s     aus

AN:       MOVE.L  D1,(timeron-tabelle)(a5)     ; TIMERON
          move.l  timeroff(pc),d2
          sub.l   d2,d1
          cmp.l   #300,d1                   ; war die Pause l�nger als 1,5 sek ?
          bgt.s   n_min
          ADDQ    #1,(sec-tabelle)(a5)      ; SEC
          move    (sec-tabelle)(a5),(tm_sec-tabelle)(a5)
          bra.s    old4

aus:      move.l  d1,(timeroff-tabelle)(a5)     ; timeroff
          move.l  timeron(pc),d2
          sub.l   d2,d1                   ; stelle Fest, ob es eine 1
          cmp.l   #25,d1                  ; > 25/200=0.12 sek
          SGT     (sig-tabelle)(a5)       ; dann 1 sonst 0
          move    sec(pc),d2
          lea     dcfbit1(pc),a1
          cmp     #32,d2
          bge.s     t2
          lea     dcfbit0(pc),a1
t2:       bclr    #5,d2
          move.l  (a1),d3
          bclr    d2,d3
          TST     (sig-tabelle)(a5)         ;SIG
          beq.s   \t3
          bset    d2,d3
\t3       move.l  d3,(a1)

OLD4:     movem.l (sp)+,d0-a6
         ; move.l  4(sp),a0
JUMP      dc.w    $4ef9       ; JMP
OLD_JOY:  dc.l    $deadface

; Hier, wenn Minutensignal empfangen

N_MIN:    SF   (sync-tabelle)(a5) ;SYNC  Zeit ung�ltig
          bsr.s   time_valid     ; Datencheck
          bne.s   \FEHLER        ; falls �bertragungsfehler
          BSR     FEED_STRUCT    ; ZEIT SETZEN

          move.l (dcfbit1-tabelle)(a5),XDCF0
          move.l (dcfbit0-tabelle)(a5),XDCF1
          move.l  #'dcf7',XMAGIC  ; Gemdos-Zeit ist jetzt g�ltig

          st     (sync-tabelle)(a5)     ;sync  aktuelle Zeit ist jetzt g�ltig
\FEHLER:  clr    (sec-tabelle)(a5)      ;sec
          bra.s     old4

time_valid:   move.l  (dcfbit0-tabelle)(a5),d1
              move.l  (dcfbit0-2-tabelle)(a5),d2
              move.l  (dcfbit1-tabelle)(a5),d3
              btst    #0,d1  ; Bit 0 = 0 ?
              bne.s    \err
              btst    #20,d1  ; bit 20 = 1 ?
              beq.s    \err
              btst    #19,d1
              bne.s   \schaltsec
              CMP     #58,(sec-tabelle)(a5)  ;SEC volle Minute empfangen ?
              bne.s   \err
              bra.s   \weiter
\schaltsec    cmp     #59,(sec-tabelle)(a5)  ; Mit Schaltsekunde 59
              bne.s   \err
              btst    #59-32,d3              ; Schaltsekunde=0 ?
              bne.s   \err
\WEITER:      move.l  d2,d0
              lSR.L   #5,D0     ; PARITY 21-28
              and.l   #$FF,d0
              bsr.s   parity
              bne.s   \err
              move.l  d2,d0
              lsr.l   #8,d0
              lsr.l   #5,d0 ; Parity 29-35
              and.l   #$7f,d0
              bsr.s   parity
              bne.s   \err
              move.l  d3,d0
              lsr.l   #36-32,d0  ; Parity 36-58
              and.l   #$7FFFFF,d0
              bsr.s   parity
              bne.s   \err
              moveq   #0,d0
              rts
\err:         moveq   #-1,d0
              rts
parity:       moveq   #31,d5
              moveq   #0,d4
\hook         btst    d5,d0
              beq.s   \not
              not     d4
\not          dbra    d5,\hook
              tst     d4
              rts
feed_struct:
             move.l (dcfbit0-2-tabelle)(a5),d0

             move   d0,d1                   ; Zeitzone und Sflag isolieren
             and    #%11111,d1
             move   d1,(tm_isdst-tabelle)(a5)
             lSR.L  #5,D0
             move   d0,d1
w2:         ; Erkennungslabel
             move   d1,d2                   ; Minute
             and    #$F,d2
             lsr    #4,d1
             and    #$7,d1
             mulu   #10,d1
             add    d2,d1
             move   d1,(tm_min-tabelle)(a5)

             lSR.L  #8,D0                   ; Stunde
             move   d0,d2
             and    #$F,d2
             lsr    #4,d0
             and    #$3,d0
             mulu   #10,d0
             add    d2,d0
             move   d0,(tm_hour-tabelle)(a5)

             move.l (dcfbit1-tabelle)(a5),d0     ; Tag
             lSR.L  #4,D0
             move   d0,d1
             move   d1,d2
             and    #$F,d2
             lsr    #4,d1
             and    #$3,d1
             mulu   #10,d1
             add    d2,d1
             move   d1,(tm_mday-tabelle)(a5)

             lSR.L  #6,D0                     ; Wochentag
             move   d0,d1
             and    #%111,d1
             move   d1,(tm_wday-tabelle)(a5)

             lSR.L  #3,D0                  ; Monat
             move   d0,d1
             move   d1,d2
             and    #$F,d2
             lsr    #4,d1
             and    #$1,d1
             mulu   #10,d1
             add    d2,d1
             move   d1,(tm_mon-tabelle)(a5)

             lSR.L  #5,D0                   ; Jahr
             move   d0,d2
             and    #$F,d2
             lsr    #4,d0
             and    #$F,d0
             mulu   #10,d0
             add    d2,d0
             move   d0,(tm_year-tabelle)(a5)
SETTIME:                                     ; Jetzt ins Gemdos-Format
          move    (tm_hour-tabelle)(a5),d0 ; Stunde*2048   +
          lsl     #6,d0
          or      (tm_min-tabelle)(a5),d0  ; Minute*32     +
          lsl     #5,d0
                                           ; (sekunde = 0)
          move.l   timea(pc),a0
          move     d0,(a0)       ; 378a / 60be
          move     d0,XTIME
SETDATE:
          move     (tm_year-tabelle)(a5),d0  ;( Jahr
          sub      #80,d0                    ; - 1980 )
          lsl      #4,d0                     ; *512   +
          or       (tm_mon-tabelle)(a5),d0   ; Monat * 32 +
          lsl      #5,d0
          or       (tm_mday-tabelle)(a5),d0  ; Tag

          move.l datea(pc),a0      ; Datum in Gemdos-Bereich setzen
          move   d0,(a0)           ; $60be
          move   d0,XDATE          ; und in Sysvar
          RTS
 end
 